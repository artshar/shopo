<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Паспорт жителя");
?><?$APPLICATION->IncludeComponent("bitrix:main.profile", ".default", Array(
	"USER_PROPERTY_NAME" => "",	// Название закладки с доп. свойствами
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"USER_PROPERTY" => "",	// Показывать доп. свойства
		"SEND_INFO" => "Y",	// Генерировать почтовое событие
		"CHECK_RIGHTS" => "N",	// Проверять права доступа
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
	),
	false
);?>
<?
global $USER;
$arGroups = $USER->GetUserGroupArray();
?>
<?if(!in_array(19,$arGroups)):?>
<?
	if($_REQUEST['become_a_dealer'] == 'yes') {
		global $USER;
		$arGroups[] = 19;
		CUser::SetUserGroup($USER->GetID(), $arGroups);
		$USER->SetUserGroupArray($arGroups);
		$USER->Update($USER->GetID(), array("GROUP_ID"=>$arGroups));
		$USER->Authorize($USER->GetID());
		LocalRedirect($APPLICATION->GetCurPage());
	}
?>
<h2>Оптовикам</h2>
<p>Нажмите эту кнопку для того, чтобы стать оптовым покупателем и получать дополнительные скидки!</p>
<p><a class="button" href="?become_a_dealer=yes">Стать оптовым покупателем</a></p>
<p>Обратите внимание, что становясь оптовым покупателем, Вы соглашаетесь с <a href="/servis/optovye-zakupki/">условиями обслуживания оптовых покупателей</a>.</p>
<?else:?>
<h2>Вы являетесь оптовым покупателем</h2>
<p>Для Вас действуют специальные цены при оптовой покупке в нашем магазине.</p>
<p>Пожалуйста, ознакомьтесь с <a href="/servis/optovye-zakupki/">условиями обслуживания</a>.</p>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>