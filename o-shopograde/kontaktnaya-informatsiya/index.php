<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
///$APPLICATION->SetPageProperty("noindex", "Y");
$APPLICATION->SetTitle("Контактная информация");
?> 
<p class="MsoNormal">Головной офис интернет магазина <span lang="EN-US">SHOPOGRAD</span><span lang="EN-US"> </span>располагается в Китае, город Гуанчжоу<o:p></o:p></p>
 
<p class="MsoNormal">
  <br />125412, г. Москва, ул. Ангарская, дом 53, кор.1
</p>
 <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=lxcHXKoCZElkpUUS7f4NnfKjLvKgfzRM&width=500&height=400&lang=ru_RU&sourceType=constructor"></script>
<p class="MsoNormal"><span lang="EN-US"> </span></p>
 
<p class="MsoNormal">Отдел продаж:</p>
 
<p class="MsoNormal"><o:p></o:p></p>
 
<p class="MsoNormal">Контактный номер: <o:p></o:p></p>
 
<p class="MsoNormal">Для звонков по России: <o:p></o:p></p>
 
<p class="MsoNormal"><span lang="EN-US">E</span>-<span lang="EN-US">mail</span>: <span lang="EN-US" style="font-family: Tahoma, sans-serif;"><a href="mailto:info@shopograd.ru" >info<span lang="RU">@</span>shopograd<span lang="RU">.</span>ru</a></span><o:p></o:p></p>
 
<p class="MsoNormal"><o:p> </o:p></p>
 
<p class="MsoNormal"><span style="font-family: Tahoma, sans-serif;">Администрация интернет магазина</span><span style="font-family: Tahoma, sans-serif;">:<span class="apple-converted-space"> </span></span><span lang="EN-US" style="font-family: Tahoma, sans-serif;"><a href="mailto:info@shopograd.ru" style="box-sizing: border-box;" ><span style="box-sizing: border-box;">info<span lang="RU">@</span>shopograd<span lang="RU">.</span>ru</span></a></span></p>
 
<p class="MsoNormal" style="box-sizing: border-box; background: white;"><o:p></o:p></p>
 
<p class="MsoNormal" style="box-sizing: border-box; background: white;"><span style="font-family: Tahoma, sans-serif;">Компания SHOPOGRAD INC (регистрационный номер компании: 1828030)</span><o:p></o:p></p>
<p class="MsoNormal" style="box-sizing: border-box; background: white;"><span style="font-family: Tahoma, sans-serif;">Мы в социальных сетях:
</span></p>
<ul>
<li><a href="https://www.facebook.com/groups/shopograd/" rel="nofollow">Facebook</a></li>
<li><a href="https://vk.com/shopograd" rel="nofollow">Вконтакте</a></li>
<li><a href="https://twitter.com/shopograd2015" rel="nofollow">Twitter</a></li>
<li><a href="https://ru.pinterest.com/shopograd/" rel="nofollow">Pinterest</a></li>
<li><a href="https://instagram.com/shopograd" rel="nofollow">Instagram</a></li>
<li><a href="https://plus.google.com/u/0/104675490679382753979/posts" rel="nofollow">Google</a></li>
<li><a href="http://ok.ru/group/52751987966040" rel="nofollow">Одноклассники</a></li>
</ul>
<p class="MsoNormal"><o:p> </o:p></p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>