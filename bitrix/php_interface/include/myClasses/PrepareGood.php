<?php

/**
 * Created by PhpStorm.
 * User: shara
 * Date: 27.04.2016
 * Time: 19:07
 */
class PrepareGood
{
    /**
     * @param array $chars
     * @return mixed
     */
    public static function processChar(array $chars)
    {
        return array_reduce(
            $chars,
            function ($carry, $item) {
                $carry[$item['VALUE']] = $item['DESCRIPTION'];
                return $carry;
            },
            []
        );

    }


    /**
     * @param $match
     * @param array $processedChars
     * @return array
     */
    public static function processMatch($match, array $processedChars)
    {
        $processedMatch = explode(',', $match);

        $string_to_find = "/{[^}]+}/";
        $processedMatch = array_map(
            function ($item) use ($processedChars, $string_to_find) {
                $matches = Array();
                preg_match_all($string_to_find, trim($item), $matches);
                if (count($matches) > 0) {
                    $matches = $matches[0];

                    foreach ($matches as $match) {
                        $replace_index = trim($match, "{}");
                        $replace = is_set($processedChars[$replace_index]) ? $processedChars[$replace_index] : '';
                        $item = str_replace($match, $replace, $item);
                    }
                }
                return $item;
            },
            $processedMatch
        );

        foreach ($processedMatch as $replace_index) {
            if (is_set($replace_index) && strlen($replace_index) > 0) {
                if (is_set($processedChars[$replace_index]) && strlen($processedChars[$replace_index]) > 0) {
                    return $processedChars[$replace_index];
                }
            }
        }

        return $processedChars[$match];
    }

    public static function strunique($str)
    {
        return implode(' ', array_unique(explode(' ', $str)));
    }

    /**
     * @param $template
     * @param array $chars
     * @return mixed
     */
    public static function nameFromTemplate($template, array $chars)
    {
        if (strlen($template) > 0 && is_array($chars)) {
            $NAME = $template;
            $string_to_find = "/#[^#]+#/";
            $matches = Array();
            preg_match_all($string_to_find, $NAME, $matches);
            if (count($matches) > 0) {
                $processedChars = self::processChar($chars);
                $matches = $matches[0];
                foreach ($matches as $match) {
                    $trimmed_match = trim($match, "#n");
                    $replace = self::processMatch($trimmed_match, $processedChars);
                    $replace = self::translate($replace);
                    $NAME = str_replace($match, $replace, $NAME);
                }
            }
            return self::strunique($NAME);
        }
    }

    public static function translate($term)
    {
        $dictionary = [
            'Другое' => '',
            'IPhone' => 'Apple',

            '()' => ''
        ];
        if (strpos($term, 'other')) return '';
        $term=str_replace( ['( содержать )','()','выше'],'',$term);
        if (is_set($dictionary[$term])) $term = $dictionary[$term];
        return $term;
    }
}