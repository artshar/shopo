<?
$MESS ['SOFTEFFECT_ACTION_PROPS'] = "обработка свойств";
$MESS ['SOFTEFFECT_SELECT_PROP'] = "Выберите сво-во";
$MESS ['SOFTEFFECT_PREVIEW_TEXT'] = "Описание для анонса";
$MESS ['SOFTEFFECT_PREVIEW_TEXT_EXT'] = "Описание для анонса (фраза в начало/конец)";
$MESS ['SOFTEFFECT_DETAIL_TEXT'] = "Детальное описание";
$MESS ['SOFTEFFECT_DETAIL_TEXT_EXT'] = "Детальное описание (фраза в начало/конец)";
$MESS ['SOFTEFFECT_OTHER_PRICE'] = "Задать цену/валюту";
$MESS ['SOFTEFFECT_UPDATE_PRICE'] = "Увеличить цену на %";
$MESS ['SOFTEFFECT_OTHER_QUANTITY'] = "Кол-во на складе";
$MESS ['SOFTEFFECT_OTHER_QUANTITY_INPUT'] = "Количество на складе:";
$MESS ['SOFTEFFECT_OTHER_QUANTITY_TRACE_INPUT'] = "Кол-во при заказе:";
$MESS ['SOFTEFFECT_OTHER_QUANTITY_TRACE_NOT_CNAHGE'] = "не изменять значение";
$MESS ['SOFTEFFECT_OTHER_QUANTITY_TRACE_Y'] = "уменьшать количество";
$MESS ['SOFTEFFECT_OTHER_QUANTITY_TRACE_N'] = "не уменьшать количество";
$MESS ['SOFTEFFECT_DETAIL_PICTURE'] = "Детальная картинка";
$MESS ['SOFTEFFECT_PREVIEW_PICTURE'] = "Картинка анонса";
$MESS ['SOFTEFFECT_PICTURE_FILE'] = "Выберите файл:";
$MESS ['SOFTEFFECT_TAGS'] = "Теги";
$MESS ['SOFTEFFECT_PICTURE_DESCR'] = "Описание файла:";
$MESS ['SOFTEFFECT_SORT'] = "Индекс сортировки";
$MESS ['SOFTEFFECT_TEXT_EXT_INSERT'] = "Вставить&nbsp;в&nbsp;";
$MESS ['SOFTEFFECT_TEXT_EXT_BEG'] = "начало";
$MESS ['SOFTEFFECT_TEXT_EXT_END'] = "конец";
$MESS ['SOFTEFFECT_TEXT_EXT_PHRASE'] = "фразу";
$MESS ['SOFTEFFECT_NACENKA'] = "Наценка";
$MESS ['SOFTEFFECT_CURRENCY'] = "Валюта";
$MESS ['SOFTEFFECT_PRICE_TYPE'] = "Тип цены:";
$MESS ['SOFTEFFECT_UPDATE_PRICE_SKU'] = "Изменять цену у торговых предложений (если есть)";
$MESS ['SOFTEFFECT_PRICE_TYPE_NO'] = "Не установлено";
$MESS ['SOFTEFFECT_NDS'] = "НДС включен в цену";
$MESS ['SOFTEFFECT_NDS_YES'] = "да";
$MESS ['SOFTEFFECT_ELEMENT'] = 'Элемент';
$MESS ['SOFTEFFECT_CATALOG'] = 'Торговый каталог';
$MESS ['SOFTEFFECT_PROPS'] = 'Свойства элемента';
$MESS ['SOFTEFFECT_PURCHASING_PRICE'] = 'Закупочная цена';
$MESS ['SOFTEFFECT_MEASURE'] = 'Единица измерения';
$MESS ['SOFTEFFECT_MEASURE_RATIO'] = 'Коэффициент единицы измерения';
$MESS ['SOFTEFFECT_SIZE'] = 'Размеры';
$MESS ['SOFTEFFECT_SIZE_LENGTH'] = 'Длина (мм.)';
$MESS ['SOFTEFFECT_SIZE_WIDTH'] = 'Ширина (мм.)';
$MESS ['SOFTEFFECT_SIZE_HEIGHT'] = 'Высота (мм.)';
$MESS ['SOFTEFFECT_SIZE_WEIGHT'] = 'Вес (грамм)';
$MESS ['SOFTEFFECT_MULTIPLE_CLEAR'] = 'заменить этими значениями существующие';
$MESS ['SOFTEFFECT_MULTIPLE_UPDATE'] = 'добавить эти значения к существующим';

$MESS['SOFTEFFECT_PROPS_DEMO_MODE_2'] = 'Модуль работает в демо-режиме. Вы можете приобрести его перейдя по <a target="_blank" href="http://softeffect.ru/basket/include/basket_add_ajax.php?ID=445562&QTY=1&action=add&ext=Y">ссылке</a>.';
$MESS['SOFTEFFECT_PROPS_DEMO_MODE_3'] = 'К сожалению демо-период закончился, надеемся вам понравился наш модуль. Вы можете приобрести его перейдя по <a target="_blank" href="http://softeffect.ru/basket/include/basket_add_ajax.php?ID=445562&QTY=1&action=add&ext=Y">ссылке</a>.';
$MESS['SOFTEFFECT_PROPS_DIFFDAYS'] = '<p style="margin-top: 0;">Ваша лицензия на модуль "#MODULE_NAME#" закончилась <b>#DIFF_DAYS#</b> дней назад.<br />Вы можете бесплатно получить продление лицензии написав положительный отзыв на странице решения перейдя по <a href="http://marketplace.1c-bitrix.ru/solutions/softeffect.props/">ссылке</a>.</p>
<p style="margin-bottom: 0;">Перед отправкой отзыва скопируйте его и отправьте нам на <a href="mailto:sale@softeffect.ru">sale@softeffect.ru</a> с темой <b>Отзыв</b>.<br />Его текст, а также имя автора, чтобы мы могли провести соответствие между вами и отзывом.<br />После модерации отзыва и его опубликования мы вышлем вам подарочный купон!</p>';

$MESS['SOFTEFFECT_PROPS_MODULE_RED'] = '<b>Перед изменениями не забудьте сделать <a href="/bitrix/admin/dump.php?lang=ru" target="_blank">резервную копию</a></b>';

$MESS ['SOFTEFFECT_MODULE_LINK'] = '<b>Благодарим вас за выбор нашего решения.</b><br />
Если, вам понравилось решение мы будем признательны вам за <b>отзыв или пожелание</b>, которые вы можете оставить перейдя <b>по <a href="http://marketplace.1c-bitrix.ru/solutions/softeffect.props/#comments" target="_blank">ссылке</a></b>. Помните, что ваши  отзывы самым лучшим образом влияют на развитие решения!<br />
Вопросы по работе модуля вы также можете задать перейдя по <a href="http://marketplace.1c-bitrix.ru/solutions/softeffect.props/#comments" target="_blank">ссылке</a>.<br />';
?>