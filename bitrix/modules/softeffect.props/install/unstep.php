<?if(!check_bitrix_sessid()) return;?>
<?
UnRegisterModuleDependences('main', 'OnAdminListDisplay', 'softeffect.props', 'CSEProps', 'OnAdminListDisplayHandler');
UnRegisterModuleDependences('main', 'OnBeforeProlog', 'softeffect.props', 'CSEProps', 'OnBeforePrologHandler');
DeleteDirFilesEx($_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/include/softeffect.props/');
UnRegisterModule('softeffect.props');

echo CAdminMessage::ShowNote(GetMessage("MOD_UNINST_OK"));
?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
	<input type="hidden" name="lang" value="<?echo LANG?>">
	<input type="submit" name="" value="<?echo GetMessage("MOD_BACK")?>">
<form>
