<?
if(!check_bitrix_sessid()) return;
IncludeModuleLangFile(__FILE__);


RegisterModuleDependences('main', 'OnAdminListDisplay', 'softeffect.props', 'CSEProps', 'OnAdminListDisplayHandler');
RegisterModuleDependences('main', 'OnBeforeProlog', 'softeffect.props', 'CSEProps', 'OnBeforePrologHandler');
CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/softeffect.props/install/ajax/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/include/', true, true);
RegisterModule('softeffect.props');

echo CAdminMessage::ShowNote(GetMessage("MOD_INST_OK"));
?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
	<input type="hidden" name="lang" value="<?echo LANG?>">
	<input type="submit" name="" value="<?echo GetMessage("MOD_BACK")?>">	
<form>