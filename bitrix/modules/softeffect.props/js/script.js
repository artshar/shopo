var sListTable = '';
var sStartCh = '';
var sEndCh = '';

$(document).ready(function(){
	$('.list input').live('click', function(e){
		if ($(this).attr('name') == 'ID[]')
		{
			if (sStartCh.length>0 && e.shiftKey && sStartCh!=$(this).val())
				sEndCh = $(this).val();
			else
				sStartCh = $(this).val();

			var bWasChecked = false;
			var bDoCheck = false;

			if (e.shiftKey && sStartCh.length>0 && sEndCh.length>0)
			{
				$('.list input').each(function(){

					if ($(this).attr('name') == 'ID[]')
					{
						if ($(this).val()==sStartCh || $(this).val()==sEndCh)
						{
							bDoCheck = !bDoCheck;
							bWasChecked = true;
						}
						if (bDoCheck)
						{
							this.checked = true;
							obListTable = new JCAdminList(sListTable);
							obListTable.SelectRow(this);
							$(this).attr('checked', true);
						}
					}
				});
			}

			if (bWasChecked)
				sStartCh = sEndCh = '';

		}
	});
});

function se_props_selected(propID, iblockID) {
	if (propID != 'null') {
		jsAjaxUtil.InsertDataToNode('/bitrix/php_interface/include/softeffect.props/ajax.php?propID=' + propID + '&iblockID=' + iblockID + '&ajax=Y', 'seprops_dest_l2', true);
	} else {
		document.getElementById('seprops_dest_l2').innerHTML='';
	}
}
