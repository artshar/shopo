<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? //test_dump($arResult);
CModule::IncludeModule('iblock');
if (count($arResult["SECTIONS"])):
else:
    ?>
    <div class="catalogue_filter side_menu" id="catalogue_filter">
        <h3>Подбор по параметрам</h3>

        <form action="<?= $APPLICATION->GetCurPage(); ?>">
            <? if ($arParams['_GLOBAL_SEARCH_TYPE']):?>
                <input type="hidden" name="GLOBAL_SEARCH_TYPE" value="<?= $arParams['_GLOBAL_SEARCH_TYPE'] ?>"/>
            <?endif; ?>
            <? if ($arParams['_GLOBAL_SEARCH_CONDITION']):?>
                <input type="hidden" name="GLOBAL_SEARCH_CONDITION"
                       value="<?= $arParams['_GLOBAL_SEARCH_CONDITION'] ?>"/>
            <?endif; ?>
            <div class="row collapse">
                <div class="small-12 medium-12 large-7 columns">
                    <div class="item">
                        <?
                        CModule::IncludeModule("currency");
                        $filter_set = false;
                        $rsEnum = CUserFieldEnum::GetList(array(), array("ID" => $arResult['SECTION']['UF_CURRENCY']));
                        $arEnum = $rsEnum->GetNext();
                        $section_currency = $arEnum["VALUE"];
                        $min_price = floor(CCurrencyRates::ConvertCurrency($arResult['SECTION']['UF_MIN_PRICE'], $section_currency, $arParams['_CURRENCY']));
                        $max_price = ceil(CCurrencyRates::ConvertCurrency($arResult['SECTION']['UF_MAX_PRICE'], $section_currency, $arParams['_CURRENCY']));
                        if ($arParams['_FILTER_AND_SORTING']['MIN_PRICE']) {
                            $min_price = $arParams['_FILTER_AND_SORTING']['MIN_PRICE'];
                            if ($min_price != floor(CCurrencyRates::ConvertCurrency($arResult['SECTION']['UF_MIN_PRICE'], $section_currency, $arParams['_CURRENCY']))) {
                                $filter_set = true;
                            }
                        }
                        if ($arParams['_FILTER_AND_SORTING']['MAX_PRICE']) {
                            $max_price = $arParams['_FILTER_AND_SORTING']['MAX_PRICE'];
                            if ($max_price != ceil(CCurrencyRates::ConvertCurrency($arResult['SECTION']['UF_MAX_PRICE'], $section_currency, $arParams['_CURRENCY']))) {
                                $filter_set = true;
                            }
                        }
                        ?>
                        <input type="hidden" name="SECTION_CURRENCY" value="<?= $section_currency; ?>"/>
                        Цена от <span class="inline_input"><span><?= $min_price ?></span><input type="text"
                                                                                                value="<?= $min_price; ?>"
                                                                                                name="MIN_PRICE"/></span>
                        до <span class="inline_input"><span><?= $max_price; ?></span><input type="text"
                                                                                            value="<?= $max_price; ?>"
                                                                                            name="MAX_PRICE"/></span> <? if ($arParams['_CURRENCY'] == 'RUB'):?>
                            <span class="rub_sign"><span class="hyphen"></span><span class="hyphen2"></span><span
                                    class="ruble">p</span><span class="dot">уб.</span></span><?
                        elseif ($arParams['_CURRENCY'] == 'USD'):?>$<?
                        else:?><?= $arParams['_CURRENCY'] ?><?endif; ?>
                    </div>

                    <? for ($i = 1; $i <= 5; $i++):?>
                        <? if (strlen($arResult['SECTION']['UF_FILTER_FIELD_' . $i])):?>
                            <?
                            $any_text = 'любой';
                            if (strlen($arResult['SECTION']['UF_FILTER_FIELD_' . $i . '_A'])) {
                                $any_text = $arResult['SECTION']['UF_FILTER_FIELD_' . $i . '_A'];
                            }
                            $current_val_text = $any_text;
                            if ($arParams['_FILTER_AND_SORTING']['FILTER_FIELD_' . $i]) {
                                $current_val_text = $arParams['_FILTER_AND_SORTING']['FILTER_FIELD_' . $i];
                                $filter_set = true;
                            }
                            ?>
                            <div class="item">
                                <?= $arResult['SECTION']['UF_FILTER_FIELD_' . $i] ?>
                            </div>
                            <div class="inline_select">
                                <span><?= $current_val_text ?></span> <img src="<?= $templateFolder; ?>/img/arr.png"/>
                                <select name="FILTER_FIELD_<?= $i ?>">
                                    <option value=""
                                            data-text="<?= $any_text ?>"<? if (!$arParams['_FILTER_AND_SORTING']['FILTER_FIELD_' . $i]):?> selected<?endif; ?>>
                                        ---<?= $any_text ?>---
                                    </option>
                                    <? foreach ($arResult['SECTION']['UF_FILTER_FIELD_' . $i . '_V'] as $v):?>
                                        <option value="<?= $v ?>"
                                                data-text="<?= $v ?>"<? if ($arParams['_FILTER_AND_SORTING']['FILTER_FIELD_' . $i] == $v):?> selected<?endif; ?>><?= $v ?></option>
                                    <?endforeach; ?>
                                </select>
                            </div>
                        <?endif; ?>
                    <?endfor; ?>
                    <? if ($filter_set):?>
                        <div class="item">
                            <a href="<?= $APPLICATION->GetCurPageParam('', array('SECTION_CURRENCY', 'MIN_PRICE', 'MAX_PRICE', 'FILTER_FIELD_1', 'FILTER_FIELD_2', 'FILTER_FIELD_3', 'FILTER_FIELD_4', 'FILTER_FIELD_5')); ?>">показать
                                все</a>
                        </div>
                    <?endif; ?>
                </div>
            </div>
        </form>
    </div>
    <div class="gap40"></div>
    <?
endif;
?>